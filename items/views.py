from django.shortcuts import render, get_object_or_404

from django.db.models import QuerySet

from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.conf import settings
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
# Create your views here.

# This is a little complex because we need to detect when we are
# running in various configurations
from items.models import Author, Book, Item, Fragment_Item, Fragment, Author_Item
from items.forms import ItemDescriptionForm, AuthorForm, ItemForm

class HomeView(View):
    def get(self, request):
        print(request.get_host())
        host = request.get_host()
        islocal = host.find('localhost') >= 0 or host.find('127.0.0.1') >= 0
        context = {
            'installed': settings.INSTALLED_APPS,
            'islocal': islocal
        }
        return render(request, 'home/main.html', context)


def view_items(request):
    context = {}

    items = Item.objects.all()
    template = "items/items.html"
    context["items"] = items

    return render(request, template_name=template, context=context)


def view_item(request, item_id: int):
    context = {}
    template = "items/item.html"

    item = get_object_or_404(Item, pk=item_id)
    fragments = get_item_fragments(item_id)
    authors = get_authors_of_an_item(item_id)
    
    context["item"] = item
    context["fragments"] = fragments
    context["authors"] = authors

    return render(request, template_name=template, context=context)


#TODO: add methods to handle this mess. One to add item when author exists and another, when he exists
# and what if item exists? - Hmm There can be a book with title the same as the other. 
def add_item(request):
    template = "items/add_item.html"
    context = {}
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = Item(title=form.cleaned_data["title"])
            item.save()
            if author_exists(name=form.cleaned_data["author"]) is False:
                author = Author(name=form.cleaned_data["author"])
                author.save()
                author_item = Author_Item(author=author, item=item)
                author_item.save()
            else:
                author = Author.objects.filter(name=form.cleaned_data["author"]).get()
                author_item = Author_Item(author=author, item=item)
                author_item.save()
                context["form"] = form
                return render(request, template_name=template, context=context)



            return HttpResponseRedirect("/items/")
    else:
        form = ItemForm()
        context["form"] = form
        return render(request, template_name=template, context=context)


def author_exists(name: str) -> bool:
    if Author.objects.filter(name=name):
        return True
    
    return False


def delete_item(request, item_id: int):
    item = get_object_or_404(Item, pk=item_id)
    item.delete()

    return HttpResponseRedirect("/items/")


def update_item(request, item_id: int):
    template = "items/update_item.html"
    context = {}
    item = get_object_or_404(Item, pk=item_id)
    author = get_authors_of_an_item(item_id)
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item.title = form.cleaned_data["title"]
            item.save()
            return HttpResponseRedirect(f"/items/item/{item_id}")
    else:
        form = ItemForm()
        form.fields["title"].initial = item.title
        form.fields["author"].initial = author.first().name
        context["form"] = form
        context["item"] = item

        return render(request, template_name=template, context=context)


def view_authors(request):
    template = 'items/authors.html'
    authors_list = Author.objects.all() 
    context = {'authors_list': authors_list}

    return render(request, template_name=template, context=context)


def view_author(request, author_id: int):
    context = {}
    author = get_object_or_404(Author, pk=author_id)
    items = get_items_of_an_author(author_id)
    template = 'items/author.html'

    context = {'author': author, 'items': items}
    
    return render(request, template_name=template, context=context)


def add_author(request):
    template = "items/add_author.html"
    context = {}
    if request.method == "POST":
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = Author(name=form.cleaned_data["name"])
            author.save()
            return HttpResponseRedirect("/items/authors/")
    else:
        form = AuthorForm()
        context["form"] = form
        return render(request, template_name=template, context=context)


def update_author(request, author_id: int):
    author = get_object_or_404(Author, pk=author_id)
    context = {}
    template = "items/update_author.html"

    if request.method == "POST":
        form = AuthorForm(request.POST)
        if form.is_valid():
            author.name = form.cleaned_data["name"]
            author.save()
            return HttpResponseRedirect(f"/items/author/{author_id}")

    if request.method == "GET":
        form = AuthorForm()
        form.fields["name"].initial = author.name
        context["form"] = form
        context["author"] = author
        return render(request, template_name=template, context=context)


def delete_author(request, author_id: int):
    author = get_object_or_404(Author, pk=author_id)
    author.delete()

    return HttpResponseRedirect("/items/authors/")


def get_book_fragments(book_id: int) -> QuerySet[Fragment]:
    return Fragment.objects.filter(fragment_item__item__id=book_id)


def get_item_fragments(item_id: int) -> QuerySet[Fragment]:
    return Fragment.objects.filter(fragment_item__item__id=item_id)


def get_items_of_an_author(author_id: int) -> QuerySet[Item]:
    return Item.objects.filter(author_item__author__id=author_id)


def get_authors_of_an_item(item_id: int) -> Author:
    try:
        return Author.objects.filter(author_item__item__id=item_id)
    except Author.DoesNotExist:
        return None


def add_description(request, item_id: int):
    item = Item.objects.get(pk=item_id)
    if request.method == "POST":
        form = ItemDescriptionForm(request.POST)
        if form.is_valid():
            item.description = form.cleaned_data["description"]
            item.summary = form.cleaned_data["summary"]
            item.save()
            return HttpResponseRedirect("/items/items/")

    else:
        form = ItemDescriptionForm()
        form.description = item.description
        form.summary = item.summary
        return render(request, "items/add_description.html", {"form": form})


def add_summary(request, item_id: int):
    item = Item.objects.get(pk=item_id)
    return HttpResponse(item.title)
