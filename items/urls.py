
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name='items'
urlpatterns=[
    path('', views.view_items,  name='all'),
    path('item/<int:item_id>',  views.view_item, name='item'),
    path('add_item/',    views.add_item, name='add_item'),
    path('delete_item/<int:item_id>',    views.delete_item, name='delete_item'),
    path('update_item/<int:item_id>',    views.update_item, name='update_item'),
    path('description/<int:item_id>',   views.add_description,  name='description'),
    path('summary/<int:item_id>',   views.add_summary,  name='summary'),
    path('author/<int:author_id>',  views.view_author,  name='author'),
    path('authors/',    views.view_authors, name='authors'),
    path('add_author/', views.add_author,   name='add_author'),
    path('delete_author/<int:author_id>',  views.delete_author,    name='delete_author'),
    path('update_author/<int:author_id>',  views.update_author,    name='update_author')
]
