from django.db import models
from django.utils import timezone

# Create your models here.


class Item(models.Model):
    title = models.CharField(max_length=80)
    description = models.TextField(null=True)
    summary = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Author(models.Model):
    name = models.CharField(max_length=100)
    items = models.ManyToManyField(Item, through="Author_Item")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Author_Item(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
                models.UniqueConstraint(fields=['author', 'item'], name="uniqueAuthorItem")
            ]


class Fragment(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Fragment_Item(models.Model):
    fragment = models.ForeignKey(Fragment, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    description = models.TextField(null=True)
    summary = models.TextField(null=True)
    done = models.BooleanField(null=True, default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
                models.UniqueConstraint(fields=['fragment', 'item'], name="uniqueFragmentItem")
            ]


class Game(Item):
    ganre = models.CharField(max_length=80)  # should be choices but tag based
    estimated_playing_time = models.IntegerField()


class Book(Item):
    ganre = models.CharField(max_length=80)  # should be choices but tag based
    pages = models.PositiveIntegerField(help_text='Enter number of pages', null=True)

