from django import forms
from items.models import Book, Item, Author


class ItemDescriptionForm(forms.Form):
    description = forms.CharField(label='description', widget=forms.Textarea)
    summary = forms.CharField(label='summary', widget=forms.Textarea)


class AuthorForm(forms.Form):
    name = forms.CharField(label='Author name')


class ItemForm(forms.Form):
    title = forms.CharField(label='Title')
    author = forms.CharField(label='Author')
