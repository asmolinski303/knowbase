import csv

from items.models import Book, Author, Item, Fragment, Fragment_Item, Author_Item


def add_book(row):
    title = row[0]
    book, created = Book.objects.get_or_create(title=title)


def add_book_and_author(row): 
    title = row[0]
    author = row[1]

    book, created = Book.objects.get_or_create(title=title)
    author, created = Author.objects.get_or_create(name=author)

    assoc, created = Author_Item.objects.get_or_create(author=author, item=book)


def add_book_fragment_and_author(row):
    title = row[0]
    fragment = row[1]
    author = row[2]

    book, created = Book.objects.get_or_create(title=title)
    fragment, created = Fragment.objects.get_or_create(name=fragment)
    author, created = Author.objects.get_or_create(name=author)

    author_book, created = Author_Item.objects.get_or_create(author=author, item=book)
    fragment_book, created = Fragment_Item.objects.get_or_create(fragment=fragment, item=book)

def run():
    with open("data/lektury2023.txt") as data:
        reader = csv.reader(data, delimiter=',', quotechar='"')
        for row in reader:
            match len(row):
                case 1: add_book(row)
                case 2: add_book_and_author(row)
                case 3: add_book_fragment_and_author(row), 
                case _: pass

